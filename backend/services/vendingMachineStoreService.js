const { findOneAndUpdate } = require('../models/vendingMachineSchema');
const vendingMachineSchema = require('../models/vendingMachineSchema');
const config = require('../configs/app');
const { errorNotFound, errorBadRequest } = require('../configs/errorMethods');
const { postLineNotification } = require('../utils/functions/lineNotification');

const service = {
  scopeSearch(req) {
    $or = [];
    if (req.query.title) $or.push({ title: { $regex: req.query.title } });
    if (req.query.description)
      $or.push({ description: { $regex: req.query.description } });
    const query = $or.length > 0 ? { $or } : {};
    const sort = { createdAt: -1 };
    if (req.query.orderByField && req.query.orderBy)
      sort[req.query.orderByField] =
        req.query.orderBy.toLowerCase() == 'desc' ? -1 : 1;
    return { query: query, sort: sort };
  },

  find(req) {
    const limit = +(req.query.size || config.pageLimit);
    const offset = +(limit * ((req.query.page || 1) - 1));
    const _q = service.scopeSearch(req);

    return new Promise(async (resolve, reject) => {
      try {
        Promise.all([
          vendingMachineSchema
            .find(_q.query)
            .sort(_q.sort)
            .limit(limit)
            .skip(offset),
          vendingMachineSchema.countDocuments(_q.query),
        ])
          .then((result) => {
            const rows = result[0],
              count = result[1];
            resolve({
              total: count,
              lastPage: Math.ceil(count / limit),
              currPage: +req.query.page || 1,
              rows: rows,
            });
          })
          .catch((error) => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  },

  findById(id) {
    return new Promise(async (resolve, reject) => {
      try {
        const obj = await vendingMachineSchema.findById(id);
        if (!obj) reject(errorNotFound('id: not found'));
        resolve(obj.toJSON());
      } catch (error) {
        reject(errorNotFound('id: not found'));
      }
    });
  },

  insert(data) {
    return new Promise(async (resolve, reject) => {
      let resVenderMachineDB = await service.findVendingMachineAndProduct(data);
      if (resVenderMachineDB) {
        reject(errorBadRequest('product duplicate'));
      } else {
        try {
          let resUpdateDB = await vendingMachineSchema.updateOne(
            {
              _id: data.vending_machine_ref_id,
            },
            {
              $push: {
                store: {
                  product_ref_id: data.product_ref_id,
                  name: data.name,
                  amount: data.amount,
                },
              },
            }
          );
          if (!resUpdateDB.modifiedCount) {
            reject(errorNotFound('vending machine not found'));
          } else {
            resolve({
              status: 200,
              message: 'success',
            });
          }
        } catch (error) {
          reject(error);
        }
      }
    });
  },

  update(id, data) {
    return new Promise(async (resolve, reject) => {
      try {
        const obj = await vendingMachineSchema.findById(id);
        if (!obj) reject(errorNotFound('id: not found'));
        await vendingMachineSchema.updateOne({ _id: id }, data);
        resolve(Object.assign(obj, data));
      } catch (error) {
        reject(error);
      }
    });
  },

  delete(id) {
    return new Promise(async (resolve, reject) => {
      try {
        const obj = await vendingMachineSchema.findById(id);
        if (!obj) reject(errorNotFound('id: not found'));
        await vendingMachineSchema.deleteOne({ _id: id });
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  },

  decreaseProduct(socketIO, data) {
    return new Promise(async (resolve, reject) => {
      let resVenderMachineDB = await service.findVendingMachineAndProduct(data);
      if (!resVenderMachineDB) {
        reject(errorBadRequest('product not found'));
      } else {
        let findProduct = resVenderMachineDB.store.find(
          (e) => String(e.product_ref_id) === data.product_ref_id
        );
        if (!findProduct) {
          reject(errorBadRequest('product not found'));
        } else {
          if (findProduct.amount <= 0) {
            reject(errorBadRequest('product sold out'));
          } else {
            try {
              let resDecreaseProduct = await vendingMachineSchema.updateOne(
                {
                  _id: data.vending_machine_ref_id,
                  'store.product_ref_id': data.product_ref_id,
                },
                {
                  $inc: {
                    'store.$.amount': -1,
                  },
                },
                {
                  returnDocument: 'after',
                }
              );
              if (resDecreaseProduct.modifiedCount) {
                if (findProduct.amount <= 11) {
                  socketIO.emit('product alert');
                  postLineNotification({
                    message: `vender machine series ${resVenderMachineDB.series_number} address ${resVenderMachineDB.location.address} product ${findProduct.name} less 10 `,
                  });
                }
                resolve({
                  status: 200,
                  message: 'success',
                });
              }
            } catch (error) {
              reject(error);
            }
          }
        }
      }
    });
  },

  findVendingMachineAndProduct({ vending_machine_ref_id, product_ref_id }) {
    return new Promise(async (resolve, reject) => {
      try {
        let resVenderMachineDB = await vendingMachineSchema.findOne({
          _id: vending_machine_ref_id,
          'store.product_ref_id': product_ref_id,
        });
        resolve(resVenderMachineDB);
      } catch (error) {
        reject(error);
      }
    });
  },
};

module.exports = { ...service };
