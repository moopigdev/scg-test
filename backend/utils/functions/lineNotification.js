const axios = require('axios');

const getConfig = () => {
  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Bearer VNNVRrk7fcTnpOz9Co30imnqdne3EEp2JI2fZEFb697`,
    },
  };
  return config;
};

const postLineNotification = (data) => {
  let params = new URLSearchParams();
  params.append('message', data.message);
  return new Promise((resolve, reject) => {
    axios
      .post(`https://notify-api.line.me/api/notify`, params, getConfig())
      .then((e) => {
        resolve(e);
      })
      .catch((err) => console.log('e', err));
  });
};

module.exports = {
  postLineNotification,
};
