var jwt = require('jsonwebtoken')
var config = require('../configs/twj')

//  FORMAT OF TOKEN
// Authorization: Bearer <access_token>

// Verify Token
let verifyToken = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['token'];
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, config.SECRET, function (err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                // console.log(req.decoded);
                console.log('success');

                next();
            }
        });

    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }
}

module.exports = verifyToken