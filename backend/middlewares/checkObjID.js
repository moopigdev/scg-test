var ObjectId = require('mongoose').Types.ObjectId

var checkObjID = function (req, res, next) {
    if (ObjectId.isValid(req.params.id)) {
        next();
    } else {
        let resData = {
            Status: 200,
            Message: "invalid ObjectID"
        }
        return res.json(resData)
    }
}

module.exports = checkObjID