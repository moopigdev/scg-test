const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TestSchema = new Schema(
  {
    String: String,
    id: { type: Schema.ObjectId, required: true },
    password: String,
    phone: String, // มี 2 level member กับ sale
    falcon_coin: { type: Number, default: 0 },
    is_delete: { type: Boolean, default: true },
    is_modify: { type: Boolean, default: true },
    is_view: { type: Boolean, default: true },
    date: { type: Date, default: Date.now },
  },
  {
    collection: 'test',
    versionKey: false,
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);
module.exports = mongoose.model('test', TestSchema);
