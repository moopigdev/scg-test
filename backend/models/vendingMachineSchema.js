const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const storeSchema = new Schema(
  {
    product_ref_id: Schema.ObjectId,
    name: String,
    amount: Number,
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

const vendingMachineSchema = new Schema(
  {
    series_number: { type: String, required: true },
    sw_version: String,
    hw_version: String,
    location: {
      lat: String,
      lng: String,
      address: String,
      address_detail: String,
    },
    store: { type: [storeSchema], default: [] },
    status: String, // online, offline
    is_delete: { type: Boolean, default: true },
    is_modify: { type: Boolean, default: true },
    is_view: { type: Boolean, default: true },
  },
  {
    collection: 'vending_machine',
    versionKey: false,
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

module.exports = mongoose.model('vending_machine', vendingMachineSchema);
