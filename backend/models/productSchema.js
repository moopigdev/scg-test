const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema(
  {
    name: String,
    price: Number,
    is_delete: { type: Boolean, default: true },
    is_modify: { type: Boolean, default: true },
    is_view: { type: Boolean, default: true },
  },
  {
    collection: 'product',
    versionKey: false,
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);
module.exports = mongoose.model('product', ProductSchema);
