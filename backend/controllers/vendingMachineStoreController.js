const service = require('../services/vendingMachineStoreService');

const controller = {
  async onGetAll(req, res) {
    try {
      const result = await service.find(req);
      res.json(result);
    } catch (error) {
      res.error(error);
    }
  },

  async onGetById(req, res) {
    try {
      const result = await service.findById(req.params.id);
      res.json(result);
    } catch (error) {
      res.error(error);
    }
  },

  async onInsert(req, res) {
    try {
      const result = await service.insert(req.body);
      res.json(result, 201);
    } catch (error) {
      res.error(error);
    }
  },

  async onDecreaseProduct(req, res) {
    try {
      const result = await service.decreaseProduct(req.io, req.body);
      res.json(result, 201);
    } catch (error) {
      res.error(error);
    }
  },

  async onUpdate(req, res) {
    try {
      const result = await service.update(req.params.id, req.body);
      res.json(result);
    } catch (error) {
      res.error(error);
    }
  },

  async onDelete(req, res) {
    try {
      await service.delete(req.params.id);
      res.json('success', 204);
    } catch (error) {
      res.error(error);
    }
  },

  async onLogin(req, res) {
    try {
      const result = await service.login(req.body);
      res.json(result);
    } catch (error) {
      res.error(error);
    }
  },
};

module.exports = { ...controller };
