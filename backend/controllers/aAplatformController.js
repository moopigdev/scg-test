const service = require('../services/aAplatformService');

const controller = {
  async onGetAll(req, res) {
    try {
      const result = await service.find(req);
      res.success(result);
    } catch (error) {
      res.error(error);
    }
  },

  async onGetById(req, res) {
    try {
      const result = await service.findById(req.params.id);
      res.success(result);
    } catch (error) {
      res.error(error);
    }
  },

  async onInsert(req, res) {
    try {
      const result = await service.insert(req.body);
      res.success(result, 201);
    } catch (error) {
      res.error(error);
    }
  },

  async onUpdate(req, res) {
    try {
      const result = await service.update(req.params.id, req.body);
      res.success(result);
    } catch (error) {
      res.error(error);
    }
  },

  async onDelete(req, res) {
    try {
      await service.delete(req.params.id);
      res.success('success', 204);
    } catch (error) {
      res.error(error);
    }
  },

  async onLogin(req, res) {
    try {
      const result = await service.login(req.body);
      res.success(result);
    } catch (error) {
      res.error(error);
    }
  },

  onRegister(req, res) {
    res.success({ page: 'login' });
  },
};

module.exports = { ...controller };
