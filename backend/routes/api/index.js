const router = require('express').Router();

router.use('/vendingMachine', require('./vendingMachineRouter'));
router.use('/vendingMachine/store', require('./vendingMachineStoreRouter'));
router.use('/product', require('./productRouter'));

module.exports = router;
