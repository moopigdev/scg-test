const express = require('express');
const router = express.Router();
const controllers = require('../../controllers/vendingMachineStoreController');

router.get('/', controllers.onGetAll);
router.get('/:id', controllers.onGetById);
router.post('/', controllers.onInsert);
router.post('/decreaseProduct', controllers.onDecreaseProduct);
router.put('/:id', controllers.onUpdate);
router.delete('/:id', controllers.onDelete);

module.exports = router;
