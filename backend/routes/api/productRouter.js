const express = require('express');
const router = express.Router();
// var auth = require('../middlewares/basicAuth')
// va = require('../middlewares/jwtToken')
// var checkObjID = require('../middlewares/checkObjID')
var controllers = require('../../controllers/productController');

// middleware that is specific to this router
// router.use(function timeLog(req, res, next) {
//   console.log('Time: ', new Date().getTime());
// //   next();
// });
router.get('/', controllers.onGetAll);
router.get('/:id', controllers.onGetById);
router.post('/', controllers.onInsert);
router.put('/:id', controllers.onUpdate);
router.delete('/:id', controllers.onDelete);

module.exports = router;
