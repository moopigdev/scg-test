const socketIO = (server, app) => {
  const io = require('socket.io')(server, { cors: { origin: '*' } });

  io.on('connection', (client) => {
    console.log('user connected');

    client.on('disconnect', () => {
      console.log('user disconnected');
    });

    // ส่งข้อมูลไปยัง Client ทุกตัวที่เขื่อมต่อแบบ Realtime
    // client.on('notification', function (message) {
    //   io.sockets.emit(message.brand, message);
    // });
  });

  // Make io accessible to our router
  app.use(function (req, res, next) {
    req.io = io;
    next();
  });
};

module.exports = socketIO;
