module.exports = {
  errorNotFound(msg) {
    return error
  },
  errorNotModified(msg) {
    let error = new Error(msg)
    error.message = msg
    error.status = 304
    return error
  },
  errorBadRequest(msg) {
    let error = new Error(msg)
    error.message = msg
    error.status = 400
    return error
  },
  errorUnauthorized(msg) {
    let error = new Error(msg)
    error.message = msg
    error.status = 401
    return error
  },
  errorPaymentRequired(msg) {
    let error = new Error(msg)
    error.message = msg
    error.status = 402
    return error
  },
  errorForbidden(msg) {
    let error = new Error(msg)
    error.message = msg
    error.status = 403
    return error
  },
  errorNotFound(msg) {
    let error = new Error(msg)
    error.message = msg
    error.status = 404
    return error
  },
  errorMethodNotAllowed(msg) {
    let error = new Error(msg)
    error.message = msg
    error.status = 405
    return error
  },
  errorUnprocessableEntity(msg) {
    let error = new Error(msg)
    error.message = msg
    error.status = 422
    return error
  },
}
