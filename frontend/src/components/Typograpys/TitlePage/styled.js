import styled from 'styled-components';

export const TitlePageStyled = styled.div`
  .tp_title {
    font-size: ${({ theme }) => theme.FONT.SIZE.S20};
  }

  .theme_standard {
  }
`;
