import React from 'react';
import cx from 'classnames';
// import PropTypes from 'prop-types'
import { TitlePageStyled } from './styled';

const TitlePage = ({ theme_standard, title }) => {
  const customClass = cx({
    theme_standard: theme_standard,
  });
  return (
    <TitlePageStyled>
      <div className={customClass}>
        <div className="tp_title">{title}</div>
      </div>
    </TitlePageStyled>
  );
};

TitlePage.propTypes = {};

export default TitlePage;
