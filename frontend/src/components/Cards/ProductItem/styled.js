import styled from 'styled-components';

export const ProductItemStyled = styled.div`
  .pi_container {
    position: relative;
    cursor: ${({ isActive }) => (isActive ? 'pointer' : 'not-allowed')};
    width: fit-content;
    border-radius: 0.25rem;
    background: ${({ theme }) => theme.COLORS.RED_1};
    padding: 0.5rem;
    overflow: hidden;
    &:hover {
      opacity: 0.9;
    }
    .pi_img {
      width: 10rem;
    }
    .pi_name {
      padding-top: 0.25rem;
      text-align: center;
      font-size: ${({ theme }) => theme.FONT.SIZE.S16};
      color: ${({ theme }) => theme.COLORS.WHITE_1};
    }
    .sold_out {
      position: absolute;
      top: 0.5rem;
      right: 0.5rem;
      border: 2px solid blue;
      background: ${({ theme }) => theme.COLORS.WHITE_1};
      padding: 0.05rem 0.25rem;
    }
  }

  .theme_standard {
  }
`;
