import React from 'react';
import cx from 'classnames';
// import PropTypes from 'prop-types'
import { ProductItemStyled } from './styled';

const ProductItem = ({ theme_standard, name, amount, onClick }) => {
  const customClass = cx({
    theme_standard: theme_standard,
  });
  return (
    <ProductItemStyled isActive={amount}>
      <div className={customClass}>
        <div
          className="pi_container"
          onClick={() => {
            amount && onClick();
          }}
        >
          <img className="pi_img" src={'/assets/mockup/can.jpg'} alt="can" />
          <div className="pi_name">
            {name} {`( ${amount} )`}
          </div>
          {!amount && <div className="sold_out">Sold Out</div>}
        </div>
      </div>
    </ProductItemStyled>
  );
};

ProductItem.propTypes = {};

export default ProductItem;
