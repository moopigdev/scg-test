import { ROUTE_PATH } from 'utils/constants';
import { Icons } from 'components';
import theme from 'styles/theme.json';
import DashboardContainer from 'containers/DashboardContainer';
import MyAssetContainer from 'containers/MyAssetSystem/MyAssetContainer';
import CoAgentDashboardContainer from 'containers/CoAgentSystems/CoAgentDashboardContainer';
import ShareAssetsContainer from 'containers/CoAgentSystems/ShareAssetsContainer';
import CoAssetsContainer from 'containers/CoAgentSystems/CoAssetsContainer';

const coAgentRoutes = [
  {
    key: 'coagent',
    path: ROUTE_PATH.COAGENT,
    name: 'coagent',
    exact: true,
    notShowSidebar: true,
    component: DashboardContainer,
  },
  {
    key: 'dashboard',
    path: ROUTE_PATH.DASHBOARD,
    name: 'Dashboard',
    exact: true,
    icon: (active) => (
      <Icons.Home color={active ? theme.COLORS.BLUE_1 : theme.COLORS.GRAY_4} />
    ),
    component: DashboardContainer,
  },
  {
    key: 'myasset',
    path: ROUTE_PATH.MY_ASSET,
    name: 'My Asset',
    exact: true,
    icon: (active) => (
      <Icons.Home color={active ? theme.COLORS.BLUE_1 : theme.COLORS.GRAY_4} />
    ),
    component: MyAssetContainer,
  },
  {
    key: 'coagent',
    path: ROUTE_PATH.DASHBOARD,
    name: 'Co Agent',
    exact: true,
    icon: () => <Icons.CoAgent color={theme.COLORS.GRAY_4} />,
    subMenu: [
      {
        key: 'coagentdashboard',
        path: ROUTE_PATH.COAGENT_DASHBOARD,
        name: 'Dashboard',
        exact: true,
        component: CoAgentDashboardContainer,
      },
      {
        key: 'coagentshareassets',
        path: ROUTE_PATH.COAGENT_SHARE_ASSETS,
        name: 'Share Assets',
        exact: true,
        component: ShareAssetsContainer,
      },
      {
        key: 'coagentcoassets',
        path: ROUTE_PATH.COAGENT_CO_ASSETS,
        name: 'Co Assets',
        exact: true,
        component: CoAssetsContainer,
      },
    ],
  },
];

export default coAgentRoutes;
