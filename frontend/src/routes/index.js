import { Route, Switch } from 'react-router-dom';
import VendorMachineContainer from 'containers/VendorMachineContainer';
// import { Mainlayout } from 'Mainlayouts';

const indexRoutes = [
  { path: '/', exact: true, name: 'page', component: VendorMachineContainer },
  // {
  //   path: '/coAgent',
  //   exact: false,
  //   name: 'coAgent',
  //   component: Mainlayout,
  // },
];

const Routes = () => {
  return (
    <Switch>
      {indexRoutes.map((prop, i) => {
        return (
          <Route
            key={i}
            exact={prop.exact}
            path={prop.path}
            component={prop.component}
          />
        );
      })}
    </Switch>
  );
};

export default Routes;
