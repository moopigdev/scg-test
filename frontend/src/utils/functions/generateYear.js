import moment from 'moment';

export const GENERATE_YEAR = () => {
  let yearCurrent = moment().format('YYYY');
  return [
    {
      label: yearCurrent - 1,
      value: yearCurrent - 1,
    },
    {
      label: yearCurrent,
      value: yearCurrent,
    },
  ];
};
