export const ROUTE_PATH = {
  LOGIN: '/',
  COAGENT: '/coagent',
  DASHBOARD: '/coagent/dashboard',
  MY_ASSET: '/coagent/myasset',
  COAGENT_DASHBOARD: '/coagent/coagentdashboard',
  COAGENT_SHARE_ASSETS: '/coagent/coagentshareassets',
  COAGENT_CO_ASSETS: '/coagent/coagentcoassets',
};
