import React, { useEffect } from 'react';
import { MainlayoutStyled } from './styled';
import { Route, Switch } from 'react-router-dom';
import 'moment/locale/th.js';
import coAgentRoutes from 'routes/coAgentRoutes';
import { NavbarWidget, SidebarWidget } from 'widgets';
// import { MemberAction, NavBar } from 'widgets';
// import { useSelector } from 'react-redux';

const Mainlayout = () => {
  // const { authenRedux } = useSelector((state) => state);
  useEffect(() => {}, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <MainlayoutStyled>
      <div className="m_top_layout">
        <NavbarWidget />
      </div>
      <div className="m_body_layout">
        <div className="m_left_col_layout">
          <SidebarWidget />
        </div>
        <div className="m_right_col_layout">
          <Switch>
            {coAgentRoutes.map((e, i) =>
              !e.subMenu ? (
                <Route
                  key={i}
                  exact={e.exact}
                  path={e.path}
                  component={e.component}
                />
              ) : (
                e.subMenu.map((f, j) => (
                  <Route
                    key={j}
                    exact={f.exact}
                    path={f.path}
                    component={f.component}
                  />
                ))
              )
            )}
          </Switch>
        </div>
      </div>
    </MainlayoutStyled>
  );
};

export default Mainlayout;
