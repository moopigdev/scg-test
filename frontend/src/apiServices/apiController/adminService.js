import apiService from '../apiService';

const apiPath = '/api/v1';

export const adminService = {
  // LOG IN
  POST_LOGIN_ADMIN: (params) => {
    return apiService.post(`${apiPath}/loginAdmin`, params);
  },

  // ADMIN
  GET_ADMIN_LIST: (stringQuery) => {
    return apiService.get(`${apiPath}/admin${stringQuery ? stringQuery : ''}`);
  },
  POST_ADMIN: (params) => {
    return apiService.post(`${apiPath}/admin`, params);
  },
  GET_ADMIN_DETAIL: (id) => {
    return apiService.get(`${apiPath}/admin/${id}`);
  },
  PUT_ADMIN: (id, params) => {
    return apiService.put(`${apiPath}/admin/${id}`, params);
  },
  DELETE_ADMIN: (id) => {
    return apiService.delete(`${apiPath}/admin/${id}`);
  },

  // MEMBER
  GET_MEMBER_LIST: (stringQuery) => {
    return apiService.get(
      `${apiPath}/member${stringQuery ? stringQuery : ''}`
    );
  },

  // ORDER
  GET_ORDER_LIST: (stringQuery) => {
    return apiService.get(`${apiPath}/order${stringQuery ? stringQuery : ''}`);
  },
  POST_ORDER: (params) => {
    return apiService.post(`${apiPath}/order`, params);
  },
  GET_ORDER_DETAIL: (id) => {
    return apiService.get(`${apiPath}/order/${id}`);
  },
  PUT_ORDER: (id, params) => {
    return apiService.put(`${apiPath}/order/${id}`, params);
  },

};
