import apiService from '../apiService';

const apiPath = '/api/v1';

export const userService = {
  // ORDER
  GET_VENDER_MACHINE_DETAIL: (id) => {
    return apiService.get(`${apiPath}/vendingMachine/${id}`);
  },
  POST_ORDER: (params) => {
    return apiService.post(
      `${apiPath}/vendingMachine/store/decreaseProduct`,
      params
    );
  },
};
