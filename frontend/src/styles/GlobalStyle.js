import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html {
    height: 100%;
    body {
      margin: 0;
      padding: 0;
      height: 100%;
      position: relative;
      font-family: prompt_regular;
      font-size: 14px;
    }
     button {
      border: none; 
      &:focus {
        outline: none;
      }
    }
    input {
      &:focus {
        outline: none;
      }
    }
  }
`;
