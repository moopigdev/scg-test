import styled from 'styled-components';

export const VendorMachineContainerStyled = styled.div`
  padding: 1rem;
  height: calc(100% - 2rem);
  .title_wrap {
    text-align: center;
    margin-bottom: 1rem;
  }
  .body_container {
    display: flex;
    padding: 0.75rem 0;
    column-gap: 1rem;
  }
`;
