import React from 'react';
import { VendorMachineContainerStyled } from './styled';
import { Typograpys, Cards } from 'components';
import { userService } from 'apiServices';
import { confirmAlert } from 'react-confirm-alert';

class VendorMachineContainer extends React.Component {
  state = {};

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    let res = await userService.GET_VENDER_MACHINE_DETAIL(
      '616fe70725827b4a7d69b211'
    );
    if (res && res.status === 200) {
      console.log('res', res);
      this.setState({
        data: res.data,
      });
    }
  };

  handleClickOrder = (e) => {
    confirmAlert({
      title: 'Confirm to submit',
      message: 'Are you sure to do this.',
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.handleClickOrderSubmit(e),
        },
        {
          label: 'No',
          // onClick: () => alert('Click No'),
        },
      ],
    });
  };

  handleClickOrderSubmit = async ({ product_ref_id }) => {
    let params = {
      vending_machine_ref_id: '616fe70725827b4a7d69b211',
      product_ref_id: product_ref_id,
      amount: 1,
    };
    let res = await userService.POST_ORDER(params);
    if (res && res.status === 200) {
      this.fetchData();
    }
  };

  render() {
    const { data } = this.state;
    return (
      <VendorMachineContainerStyled>
        <div className="title_wrap">
          <Typograpys.TitlePage title="Drinking Machine" />
        </div>
        <div className="body_container">
          {data &&
            data.store.map((e, i) => (
              <div key={i} className="item_wrap">
                <Cards.ProductItem
                  id={e._id}
                  name={e.name}
                  amount={e.amount}
                  onClick={() => this.handleClickOrder(e)}
                />
              </div>
            ))}
        </div>
      </VendorMachineContainerStyled>
    );
  }
}

export default VendorMachineContainer;
