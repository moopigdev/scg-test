import React from 'react';
import { withTranslation } from 'react-i18next';
import { AaPlatContainerStyled } from './styled';

class AaPlatContainer extends React.Component {
  handleClickChangLang = () => {
    const { i18n } = this.props;
    i18n.changeLanguage('en');
  };

  render() {
    const { t } = this.props;
    return (
      <AaPlatContainerStyled useSuspense={false}>
        {t('ok')}
        <button onClick={this.handleClickChangLang}>dddd</button>
      </AaPlatContainerStyled>
    );
  }
}

export default withTranslation('common')(AaPlatContainer);

// const mapStateToProps = (state) => ({
//   authenRedux: state.authenRedux,
//   companySelectedRedux: state.companySelectedRedux,
// });

// const mapDispatchToProps = (dispatch) => ({});

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(AaPlatContainer);