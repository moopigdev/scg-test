import { AUTHEN } from './actionTypes';

export const setReduxAuthen = (data) => {
  return {
    type: AUTHEN,
    data,
  };
};
