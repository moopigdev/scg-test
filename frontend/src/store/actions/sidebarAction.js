import { IS_SHOW_SIDEBAR } from './actionTypes';

export const setReduxIsShowSidebar = (data) => {
  return {
    type: IS_SHOW_SIDEBAR,
    data,
  };
};
